Manual:

    How to run the program:
        In the command prompt while in the repository, enter:
            python src/main.py FRACTALNAME GRADIENTNAME;
                Where FRACTALNAME is a file that contains the components:
                    type: mandelbrot, julia, or mandelbrot4
                    pixels: side length of square canvas
                    centerx: x coordinate of the center starting position
                    centery: y coordinate of the center starting position
                    axislength: amount of complex units of side length
                    iterations: amount of times the fractal formula is used, the more used the clearer the image
                Where GRADIENTNAME is either:
                    Interstellar: gradient ranges from black to white
                    DreamCatcher: gradient ranges from red to purple
                    TheBlues: gradient ranges from cyan to navy blue
                If either FRACTALNAME and GRADIENTNAME are left blank, the default will be:
                    FRACTALNAME: data/zoomed.frac
                    GRADIENTNAME: DreamCatcher
                    
        Then, the tk window will come up and you can watch the image be made!
        Afterwards, just exit the window to finish. Also note this will write the image to a file saved in the src/
        folder.
    Errors:
        If an incorrect fractal is specified, a error message will appear saying that the file could not be opened.
        If an incorrect gradient is specified, a NotImplementedError will appear saying an invalid gradient was given.
        