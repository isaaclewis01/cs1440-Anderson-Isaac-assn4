| Dates | Events |
|-------|--------|
| 3/17/20 | Cloned repository, started set up |
| 3/18/20 - 3/23/20 | Did not have time to work |
| 3/24/20 | Started list of code smells in my SDP |
| 3/25/20 | Finished SDP |
| 3/26/20 | Finished everything for 4.0! |
| 3/27/20 - 4/6/20 | Did not work on this project |
| 4/7/20 | Started 4.1, finished coding most of project |
| 4/8/20 - 4/9/20 | Did not have time to work |
| 4/10/20 | Finished, added testing for errors and updated Plan, Manual, and UML |
