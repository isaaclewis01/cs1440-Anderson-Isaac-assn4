Software Development Plan:
    
    --4.0 Plan--
    
    1. Requirements
        As a whole, this program can be reorganized into at least 6 modules:
            0: main.py
            1. Config.py
            2. Mandelbrot.py
            3. Julia.py
            4. Gradient.py
            5. ImagePainter.py
            
        Rely on tests, and make sure the code is sorted and "cleaned" without changing how it functions and outputs.
        Remember that in the future there will be editions, so make sure it will be easy to add to.
        
        Code Smells:    
            In file mbrot_fractal.py:
                Lines 10-27 contains the gradient list that is used in multiple modules:
                    ```
                    gradient = [
                        ...
                        ]
                    ```
                    This could be put in its own module/class to be imported and used elsewhere
                    
                Lines 35-6 has a comment with information that is not correct:
                    ```
                    """Paint a Fractal image into the TKinter PhotoImage canvas.
                    This code creates an image which is 640x640 pixels in size."""
                    ```
                    It actually creates an image which is 512x512 pixels in size, should be:
                    ```
                    """Paint a Fractal image into the TKinter PhotoImage canvas.
                    This code creates an image which is 512x512 pixels in size."""
                    ```
                    
                Lines 35-39 in the colorOfThePixel function have global variables that shouldn't be global:
                    ```
                    global z
                    z = complex(0, 0)  # z0
                
                    global MAX_ITERATIONS
                    global i
                    ```
                    Since global variables aren't recommended, we can instead find another way to declare and use these
                    
                Line 48 there is a useless line of code:
                    ```
                    return gradient[MAX_ITERATIONS]
                    ```
                    Deleting it will do nothing, so delete it. With this there is a comment of code that can be deleted:
                    ```
                    # XXX: one of these return statements made the program crash...
                    ```
                    
                Lines 55-56 in the paint function are gloabal:
                    ```
                    global gradient
                    global img
                    ```
                    Should be redefined so they aren't global variables
                    
                Line 65 maxy is created and never used:
                    ```
                    maxy = fractal['centerY'] + (fractal['axisLen'] / 2.0)
                    ```
                    Could be deleted or used somewhere useful
                    
                Lines 68-83 have many random numbers that are hard to see what they are associated with:
                    ```
                    canvas = Canvas(window, width=512, height=512, bg='#ffffff')
                    canvas.pack()
                    canvas.create_image((256, 256), image=img, state="normal")
                    ...
                    pixelsize = abs(maxx - minx) / 512
                    ...
                    portion = int(512 / 64)
                    total_pixels = 1048576
                    for row in range(512, 0, -1):
                        for col in range(512):
                            ...
                            img.put(color, (col, 512 - row))
                    ```
                    These can be defined in a variable, or introduced in a comment explainging what they are
                    
                Lines 76-77 have variables that are never used:
                    ```
                    portion = int(512 / 64)
                    total_pixels = 1048576
                    ```
                    Either delete or find a place where they can be used

                Lines 87-90 (pixelsWrittenSoFar) never gets used:
                    ```
                    def pixelsWrittenSoFar(rows, cols):
                        pixels = rows * cols
                        print(f"{pixels} pixels have been output so far")
                        return pixels
                    ```
                    Delete or find reason to keep
                    
                Lines 138-153 redundant, used in both mbrot_fractal.py and julia_fractal.py:
                    ```
                    if __name__ == '__main__':
                        ...
                        mainloop()
                     ```
                     Put into a new file, main.py
                     
            In file julia_fractal.py:
                Lines 16-19 has global variables and an unhelpful comment:
                    ```
                    # I feel bad about all of the global variables I'm using.
                    # There must be a better way...
                    global grad
                    global win
                    ```
                    Find different way to use variables without them being global, and remove the comment
                    
                Lines 21-22 has a comment that describes a number that is different than the one in the comment:
                    ```
                    # Here 76 refers to the number of colors in the gradient
                    for i in range(78):
                    ```
                    Change the '76' to a 78 so that the comment is helpful
                    ```
                    # Here 78 refers to the number of colors in the gradient
                    for i in range(78):
                    ```
                
                Line 26 has unreachable code:
                    ```
                    z += z + c
                    ```
                    Can be deleted without consequence
                
                Line 29 has unreachable code:
                    ```
                    return grad[78]
                    ```
                    Can be deleted without consequence
                
                Line 32 has a horribly long function name:
                    ```
                    def getFractalConfigurationDataFromFractalRepositoryDictionary(dictionary, name):
                    ```
                    Can be changed to a shorter and still discriptave name such as:
                    ```
                    def getConfigData(dictionary, name):
                    ```
                    
                Line 44 has a variable that is never used:
                    ```
                    value = dictionary[key]
                    ```
                    Can be deleted
                    
                Line 48 has a function with parameters 'i' and 'e' that are never used:
                    ```
                    def makePicture(f, i, e):
                    ```
                    These can be removed without consequence, or maybe incorporated into the function
                    
                Lines 52-54 are global variables, we don't want those:
                    ```
                    global win
                    global grad
                    global photo
                    ```
                    Find a way to use them without making them global
                    
                Line 64 has another global variable, also seems out of place:
                    ```
                    global WHITE
                    ```
                    Incorporate into program in a way so it isn't global
                    
                Lines 72-83 has "canvas.pack()" 6 times, which is more than needed:
                    ```
                    canvas.pack()
                    ...
                    canvas.pack() # This seems repetitive
                    canvas.pack() # But it is how Larry wrote it
                    canvas.pack() # Larry's a smart guy.  I'm sure he has his reasons.
                    ...
                    canvas.pack() # Does this even matter?
                    ...
                    canvas.pack()
                    ```
                    Removing all but the last should result in cleaner code while having it run correctly.
                    Also, the comments associated can be removed as well as they have no purpose.
                    
                Lines 48-91 is a function that is too long and does too much, also containing some "magic" numbers:
                    ```
                    def makePicture(f, i, e):
                        ...
                        canvas.pack()
                        ...
                    ```
                    Can seperate into multiple functions, define and rename variables for numbers
                    
                Lines 68-70 has a comment that is not useful to the program:
                    ```
                        # TODO: Sometimes I wonder whether some of my functions are trying to do
                        #       too many different things... this is the correct part of the
                        #       program to create a GUI window, right?
                    ```
                    Can be removed, especially after editing the rest
                
                Line 76 has a variable that is never used:
                    ```
                    area_in_pixels = 512 * 512
                    ```
                    Can be removed or used where helpful if possible
                
                Line 84 has a variable that is never used:
                    ```
                    fraction = int(512 / 64)
                    ```
                    Can be removed or used where helpful if possible
                    
                Lines 99-116 has a list that is is used in multiple modules in this program:
                    ```
                    gradient = [
                        ...
                        ]
                    ```
                    Can be moved to a new module so that others may reference it from there
                    
                Lines 128-147 is the dictionary f:
                    ```
                    f = {
                        ...
                    }
                    ```
                    Will eventually be in a different module, but should also be renamed to a more descriptive variable:
                    ```
                    images = {
                        ...
                    }
                    ```
                    
                Line 150, variable seems out of place:
                    ```
                    WHITE = '#ffffff'
                    ```    
                    Should be moved with other variable declerations
                    
                Lines 153-184 is repetetive among modules, like referenced above ^:
                    ```
                    if __name__ == '__main__':
                        ...
                        mainloop()
                     ```
                     Put into a new file, main.py
                
    2. Design:
        Program must have at least 6 modules, as referenced in requirements:
            0: main.py
                Driver program; imports other modules, accepts command-line arguments, and displays fractal image to
                the screen. Main entry point of the program.
            1. Config.py
                Contains Python dictionary composed of fractal configuration data.
                - Add a key 'type' to the dictionary that returns what type of fractal is to be evoked
            2. Mandelbrot.py
                Given a coordinate, return the iteration count of the Mandelbrot function for that point.
            3. Julia.py
                Given a coordinate, return the iteration count of the Julia function for that point.
            4. Gradient.py
                Contains an array G contain N colors; when the Mandelbrot or Julia function returns an iteration count
                of a point in the complex plane, the pixel is painted G[count].
            5. ImagePainter.py
                Creates a Tk window and a PhotoImage object; the PhotoImage stores the pixels and can create a PNG image
                file.
        Eventually, there will be classes, objects, inheritence, and all that jazz involved with object-oriented
        programming. Other than making this a well-factored program, the rest of the capablilities should remain. The
        output will be the same as when this program was a mess.

    3. Implementation:
        This program is located in src/ and its driver should be main.py
        User specifies which fractal to do and which part of the fractal to print a PNG of
        Unit tests will be created/adapted to test code

    4. Verification:
        Test one:
            User enters 'python src/main.py hourglass'
            Output:
                Writes the image to the screen of the 'hourglass' portion of the julia fractal
            Pass/Fail? Pass
            
        Test two:
            User enters 'python src/main.py invalidInput'
            Output:
                ERROR: moustache is not a valid fractal
                Please choose one of the following:
                    fulljulia
                    hourglass
                    lakes
                    fullmandelbrot
                    spiral0
                    spiral1
                    seahorse
                    elephants
                    leaf
            Pass/Fail? Pass
            
    --4.1 Plan--
    
    1. Requirements
        Define:
            Fractal Abstract Class with subclasses:
                Julia
                Mandelbrot
                Mandelbrot4
            Gradient Abstract Class with subclasses:
                Interstellar
                DreamCatcher
                TheBlues
            FractalFactory class
            GradientFactory class
            Keep: main and ImagePainter
                
    2. Design:
        0. main.py
            Driver program; imports sys to read the command prompt and imagePainter to paint the fractal
        1. ImagePainter
            Imports the FractalFactory, GradientFactory, and tkinter in order to paint the fractal and write it to
            a file.
        2. FractalFactory
            Imports Fractal subclasses Julia, Mandelbrot, and Mandelbrot4 in able to return a fractal object of the
            correct type.
        3. Fractal
            Abstract class that has a count(compNum) takes one complex number as input and returns an integer that is
            the number of iterations tried before the absolute value of the Fractal formula grew larger than 2.0;
            otherwise the maximum number of iterations is returned and is used as a parent class for:
                Julia
                    Defines the attributes of the Julia Fractal
                Mandelbrot
                    Defines the attributes of the Mandelbrot Fractal
                Mandelbrot4
                    Defines the attributes of the Mandelbrot^4 Fractal, where Z is taken to the power of 4 instead of
                    two.
        4. GradientFactory
            Imports Gradient subclasses Interstellar, DreamCatcher, and TheBlues in order to return a Gradient object
            of the correct type.
        5. Gradient
            Abstract class that has method getColor(n) that takes an integer (n) as input and returns a string that
            represents a color in this format: "#RRGGBB." From this, Gradient is a parent class for:
                Interstellar
                    Gradient that ranges from black to white.
                DreamCatcher
                    Gradient that ranges from red to purple (rainbow!)
                TheBlues
                    Gradient that ranges from cyan to navy blue

    3. Implementation:
        This program is located in src/ and its driver is main.py
        User specifies which .frac Fractal in the data/ folder they would like to create and the Gradient they want it
        to be colored
        Unit tests will be created/adapted to test code

    4. Verification:
        Test one:
            User enters python src/main.py
            Output:
                Paints the Fractal defined by data/zoomed.frac that is colored with the Gradient dreamCatcher, as
                defined by default in ImagePainter.
                Also, prints:
                    FractalFactory: Creating default fractal
                    GradientFactory: Creating default color gradient
            Pass/Fail? Pass
        Test two:
            User enters python src/main.py data/DNE.frac Interstellar
            Output:
                Traceback (most recent call last):
                  File "src/main.py", line 9, in <module>
                  fractal = ImagePainter.ImagePainter(background='#ffffff', fractName=sys.argv[1], gradName=sys.argv[2])
                  File "C:\Users\isaac\cs1440-Anderson-Isaac-assn4\src\ImagePainter.py", line 8, in __init__
                  self.__fractDict = FractalFactory().dictionary(fractName)
                  File "C:\Users\isaac\cs1440-Anderson-Isaac-assn4\src\FractalFactory.py", line 6, in dictionary
                  f = open(fractalName, 'r')
                  FileNotFoundError: [Errno 2] No such file or directory: 'data/DNE.frac'
            Pass/Fail? Pass
            