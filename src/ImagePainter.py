from tkinter import Tk, Canvas, PhotoImage, mainloop
from FractalFactory import FractalFactory
from GradientFactory import GradientFactory


class ImagePainter:
    def __init__(self, background, fractName='../data/zoomed.frac', gradName='DreamCatcher'):
        self.__fractDict = FractalFactory().dictionary(fractName)
        self.__fractName = fractName
        self.__gradName = gradName
        self.__size = self.__fractDict.get('pixels')
        self.__background = background
        self.__window = Tk()
        self.__img = PhotoImage(width=self.__size, height=self.__size)

    def paint(self):
        """Paint a fractal image"""

        gradient = GradientFactory(self.__fractDict.get('iterations')).makeGradient(self.__gradName)
        fractal = FractalFactory().makeFractal(self.__fractName)

        # Figure out how the boundaries of the PhotoImage relate to coordinates on
        # the imaginary plane.
        minx = self.__fractDict.get('min').get('x')
        maxx = self.__fractDict.get('max').get('x')
        miny = self.__fractDict.get('min').get('y')

        # Display the image on the screen
        canvas = Canvas(self.__window, width=self.__size, height=self.__size, bg=self.__background)
        canvas.pack()
        canvas.create_image((self.__size / 2, self.__size / 2), image=self.__img, state="normal")

        # At this scale, how much length and height on the imaginary plane does one
        # pixel take?
        pixelsize = self.__fractDict.get('pixelsize')

        for row in range(self.__size, 0, -1):
            for col in range(self.__size):
                x = minx + col * pixelsize
                y = miny + row * pixelsize
                color = gradient.getColor(fractal.count(complex(x, y)))
                self.__img.put(color, (col, self.__size - row))
            self.__window.update()

    def run(self):
        # Set up the GUI so that we can paint the fractal image on the screen
        self.paint()

        # Save the image as a PNG
        self.__img.write(self.__fractDict.get("imagename") + "-" + self.__gradName + ".png")
        print(f"Wrote image " + self.__fractDict.get("imagename") + "-" + self.__gradName + ".png")

        # Call tkinter.mainloop so the GUI remains open
        mainloop()
