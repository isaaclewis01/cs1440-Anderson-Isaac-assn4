#!/bin/env python3

# Fractal Visualizer

import sys
import ImagePainter

if len(sys.argv) < 2:
    fractal = ImagePainter.ImagePainter(background='#ffffff')
elif len(sys.argv) < 3:
    fractal = ImagePainter.ImagePainter(background='#ffffff', fractName=sys.argv[1])
else:
    fractal = ImagePainter.ImagePainter(background='#ffffff', fractName=sys.argv[1], gradName=sys.argv[2])

fractal.run()
