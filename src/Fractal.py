class Fractal:
    def __init__(self, iterations):
        raise NotImplementedError("Concrete subclass of Fractal must implement __init__")

    def count(self, compNum):
        raise NotImplementedError("Concrete subclass of Fractal must implement count() method")


class Julia(Fractal):
    def __init__(self, iterations):
        self.__iterations = iterations

    def count(self, compNum):
        fractComplex = complex(-1.0, 0.0)
        for i in range(self.__iterations):
            compNum = compNum * compNum + fractComplex  # Get fractalComplex1, fractalComplex2, ...
            if abs(compNum) > 2:
                return i
        return self.__iterations


class Mandelbrot(Fractal):
    def __init__(self, iterations):
        self.__iterations = iterations

    def count(self, compNum):
        fractComplex = complex(0, 0)
        for i in range(self.__iterations):
            fractComplex = fractComplex * fractComplex + compNum
            if abs(fractComplex) > 2:
                return i
        return self.__iterations


class Mandelbrot4(Fractal):
    def __init__(self, iterations):
        self.__iterations = iterations

    def count(self, compNum):
        fractComplex = complex(0, 0)
        for i in range(self.__iterations):
            fractComplex = fractComplex ** 4 + compNum
            if abs(fractComplex) > 2:
                return i
        return self.__iterations
