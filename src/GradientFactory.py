from Gradient import Interstellar, DreamCatcher, TheBlues


class GradientFactory:
    def __init__(self, iterations):
        self.__iterations = iterations

    def makeGradient(self, name):
        if name == 'Interstellar':
            return Interstellar(self.__iterations)
        elif name == 'DreamCatcher':
            return DreamCatcher(self.__iterations)
        elif name == 'TheBlues':
            return TheBlues(self.__iterations)
        else:
            raise NotImplementedError("Invalid gradient requested")
