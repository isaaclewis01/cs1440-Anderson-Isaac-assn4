import unittest
from Gradient import DreamCatcher, Interstellar, TheBlues
from FractalFactory import FractalFactory
from colour import Color


# autocmd BufWritePost <buffer> !python3 runTests.py

class TestJulia(unittest.TestCase):
    def test_gradient(self):
        self.assertEqual(DreamCatcher(10).getColor(-1), Color('Red'))
        self.assertEqual(DreamCatcher(10).getColor(10), Color('Purple'))


    def test_dictionary(self):
        self.assertEqual(FractalFactory().dictionary('data/fulljulia.frac').get('axislength'), 4.0)
        self.assertEqual(FractalFactory().dictionary('data/fulljulia.frac').get('iterations'), 78)


if __name__ == '__main__':
    unittest.main()
