import unittest
from Gradient import DreamCatcher, Interstellar, TheBlues
from FractalFactory import FractalFactory
from colour import Color


# autocmd BufWritePost <buffer> !python3 runTests.py

class TestMandelbrot(unittest.TestCase):
    def test_gradient(self):
        self.assertEqual(Interstellar(10).getColor(-1), Color('Black'))
        self.assertEqual(Interstellar(10).getColor(10), Color('White'))


    def test_dictionary(self):
        self.assertEqual(FractalFactory().dictionary('data/fullmandelbrot.frac').get('pixels'), 640)
        self.assertEqual(FractalFactory().dictionary('data/elephants.frac').get('iterations'), 100)


if __name__ == '__main__':
    unittest.main()
