from Fractal import Julia, Mandelbrot, Mandelbrot4


class FractalFactory:
    def dictionary(self, fractalName):
        f = open(fractalName, 'r')
        fractalList = f.read().split('\n')
        f.close()

        fractType = None
        centerX = None
        centerY = None
        axisLen = None
        pixels = None
        iterations = None

        for index in fractalList:
            if index.lower().startswith('type'):
                fractType = index.split(': ')[1].strip('\n')
            if index.lower().startswith('centerx'):
                centerX = float(index.split(': ')[1].strip('\n'))
            if index.lower().startswith('centery'):
                centerY = float(index.split(': ')[1].strip('\n'))
            if index.lower().startswith('axislen'):
                axisLen = float(index.split(': ')[1].strip('\n'))
            if index.lower().startswith('pixels'):
                pixels = int(index.split(': ')[1].strip('\n'))
            if index.lower().startswith('iterations'):
                iterations = int(index.split(': ')[1].strip('\n'))

        fractDict = {
            'type': fractType,
            'pixels': pixels,
            'axislength': axisLen,
            'pixelsize': abs((centerX + axisLen / 2) - (centerX - axisLen / 2)) / pixels,
            'iterations': iterations,
            'min': {'x': centerX - axisLen / 2, 'y': centerY - axisLen / 2},
            'max': {'x': centerX + axisLen / 2, 'y': centerY + axisLen / 2},
            'imagename': fractalName[5:-5],

        }

        # Error checking
        if fractDict.get('type') is None or fractDict.get('pixels') is None or fractDict.get('axislength') is None \
                or fractDict.get('pixelsize') is None or fractDict.get('iterations') is None or\
                fractDict.get('min').get('x') is None or fractDict.get('min').get('y') is None or\
                fractDict.get('max').get('x') is None or fractDict.get('max').get('y') is None:
            raise RuntimeError("Invalid fractal file data")

        return fractDict

    def makeFractal(self, fractalName):
        fractDict = self.dictionary(fractalName)

        if fractDict.get('type') == 'mandelbrot':
            return Mandelbrot(fractDict.get('iterations'))
        elif fractDict.get('type') == 'julia':
            return Julia(fractDict.get('iterations'))
        elif fractDict.get('type') == 'mandelbrot4':
            return Mandelbrot4(fractDict.get('iterations'))
        else:
            raise RuntimeError("Incorrect fractal type")
