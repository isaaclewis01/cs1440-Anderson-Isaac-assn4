from colour import Color


class Gradient:
    def __init__(self, iterations):
        raise NotImplementedError("Concrete subclass of Gradient must implement __init__")

    def getColor(self, n):
        raise NotImplementedError("Concrete subclass of Gradient must implement getColor() method")


class Interstellar(Gradient):
    def __init__(self, iterations):
        self.__iterations = iterations + 2
        __colors = Color('Black').range_to('White', self.__iterations)
        self.__colorList = list(__colors)

    def getColor(self, n):
        return self.__colorList[n + 1]


class DreamCatcher(Gradient):
    def __init__(self, iterations):
        self.__iterations = iterations + 2
        __colors = Color('Red').range_to('Purple', self.__iterations)
        self.__colorList = list(__colors)

    def getColor(self, n):
        return self.__colorList[n + 1]


class TheBlues(Gradient):
    def __init__(self, iterations):
        self.__iterations = iterations + 2
        __colors = Color('Cyan').range_to('NavyBlue', self.__iterations)
        self.__colorList = list(__colors)

    def getColor(self, n):
        return self.__colorList[n + 1]